# Postgres in Docker

Running PostgreSQL and pgAdmin as Docker containers. For more info see: https://nadith.me/postgres-in-docker/

Docker compose commands to start/stop the containers

```shell
# Start
docker-compose up -d

# Stop
docker-compose down
```